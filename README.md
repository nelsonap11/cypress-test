# Nelson VU

1. Instalación
Es importante seguir la documentación oficial.
       https://jestjs.io/es-ES/docs/getting-started
npm install cypress --save-dev

2. Otras configuraciones realizadas las tienes en Package.json

3. Debes tener un directorio llamado E2E, y en ellos carpetas para los test de API y UI

4. npm init para bajar las dependecias actuales

5. Para correr el proyecto usar "npm run cypress:open"

6. Objetivo: camino feliz de los test automatizados del API y UI