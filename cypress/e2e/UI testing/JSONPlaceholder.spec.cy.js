/// <reference types="cypress" />

describe('example to-do app', () => {
  beforeEach(() => {
  })
})
it('cy.visit() - JSON Placeholder', () => {
cy.wait(5000)
cy.visit('https://jsonplaceholder.typicode.com/guide', {


  })
  cy.get('.container > :nth-child(5)')
  cy.contains('Getting a resource').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(9)')
  cy.contains('Listing all resources').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(13)')
  cy.contains('Creating a resource').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(18)')
  cy.contains('Updating a resource').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(23)')
  cy.contains('Patching a resource').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(28)')
  cy.contains('Deleting a resource').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(31)')
  cy.contains('Filtering resources').click()
  cy.wait(2000)
  cy.get('.container > :nth-child(34)')
  cy.contains('Listing nested resources').click()
  cy.screenshot()
  })

